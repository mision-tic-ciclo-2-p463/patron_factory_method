public class Circulo extends Figura {
    //atributo
    private double radio;

    //Constructor
    public Circulo(double radio){
        this.radio = radio;
    }

    public double getPerimetro(){
        return 2 * Math.PI * this.radio;
    }

    public double getArea(){
        return Math.PI * Math.pow(this.radio, 2);
    }
}
